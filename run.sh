#!/bin/bash
set -e
source "/home/jovyan/shares/SR004.nfs2/amaksimova/env/bin/activate"
nohup torchrun --nproc_per_node 2 -m finetune.run \
--output_dir /home/jovyan/shares/SR004.nfs2/amaksimova/tune_bge/tune_bge/models_output/1 \
--model_name_or_path /home/jovyan/shares/SR004.nfs2/amaksimova/tune_bge_exp/models/bi_bert_pretrained \
--train_data /home/jovyan/shares/SR004.nfs2/amaksimova/tune_bge_exp/data/supervised_ru \
--learning_rate 2e-5 \
--fp16 \
--num_train_epochs 3 \
--per_device_train_batch_size 128 \
--dataloader_drop_last True \
--query_max_len 512 \
--passage_max_len 512 \
--train_group_size 8 \
--logging_steps 100 \
--gradient_checkpointing True \
--deepspeed /home/jovyan/shares/SR004.nfs2/amaksimova/tune_bge_exp/config/ds_config.json \
> out/a.out &